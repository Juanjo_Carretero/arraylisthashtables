package UD7Ej3.app;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class UD7Ej3App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		//VARIABLE PARA EL SWITCH
		int menu;
		
		//OPCIONES DE MENU
		System.out.println("1- A�adir producto / 2- Mostrar lista / 3- Busqueda de producto");
		menu = scan.nextInt();
		
		//DEFINICION DE HASTABLE
		Hashtable<String, String> stock=new Hashtable<String, String>();
		
		stock.put("Port�til", "900");
		stock.put("PC-Sobremesa", "2000");
		stock.put("Tablet", "200");
		stock.put("Smartphone", "550");
		stock.put("Gr�fica NVIDIA", "700");
		stock.put("Adaptador USB", "2");
		stock.put("RAM", "50");
		stock.put("Refrigeraci�n", "200");
		stock.put("Cable ethernet", "10");
		stock.put("Gr�fica AMD", "600");
		
		//SWITCH CON LAS OPCIONES DEL MEN�
		switch (menu) {
		case 1:
			System.out.println("Nombre de producto.");
			String nombreproducto = scan.next();
			
			System.out.println("Que precio tiene "+nombreproducto);
			String precio = scan.next();
			
			stock.put(nombreproducto, precio);
			
			mostrarStock(stock);
			
			break;
			
		case 2:
			mostrarStock(stock);
			break;
		case 3:
			System.out.println("Que producto quieres consultar");
			String consulta = scan.next();
			consultaProducto(consulta, stock);
			break;
		default:
			break;
		}
	}
	
	//M�TODO PARA MOSTRAR EL STOCK
	public static void mostrarStock(Hashtable<String, String> dicc) {
		Enumeration<String> enumeration = dicc.elements();
		Enumeration<String> llaves = dicc.keys();
		while (enumeration.hasMoreElements()) {
		  System.out.println(""+"Stock: " + llaves.nextElement() + " " + enumeration.nextElement());
		}
	}
	
	//M�TODO PARA CONSULTAR UN PRODUCTO
	public static void consultaProducto(String consulta, Hashtable<String, String> dicc) {
		System.out.println("El producto consultado tiene el precio de: "+dicc.get(consulta));
	}
}
