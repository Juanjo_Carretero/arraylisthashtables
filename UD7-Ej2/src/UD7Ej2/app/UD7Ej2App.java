package UD7Ej2.app;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class UD7Ej2App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		//DEFINO EL ARRAYLIST
		ArrayList<Double> carrito = new ArrayList<>();
		
		//DECLARACION DE VARIABLES
		boolean salir=false;
		int numeroproductos=0;
		int iva=0;
		double pagado=0;
		double cambio=0;
		double total=0;
		double totaliva=0;
		double totalmasiva=0;
		
		do {
			System.out.println("Cuanto vale su producto?");
			double precio = scan.nextDouble();
			
			carrito.add(precio);
			
			System.out.println("Quiere comprar algun producto mas. (si o no)");
			String decision = scan.next();
			
			if (decision.equals("si")) {
				salir = true;
			}else {
				salir = false;
				System.out.println("Cantidad pagada?");
				pagado = scan.nextDouble();
				
				System.out.println("IVA (21 o 4 %)");
				iva = scan.nextInt();
			}
			
			total = total + precio;
		} while (salir == true);
		
		//CALCULO EL IVA DEPENDIENDO DE SI ES AL 21% O AL 4%
		if (iva==21) {
			totaliva = (total * 21)/100;
			totalmasiva = total + totaliva;
		}else {
			totaliva = (total * 4)/100;
			totalmasiva = total + totaliva;
		}
		
		//GENERO UNA ITERACIÓN PARA PODER CONTAR EL NUMERO DE PRODUCTOS
		Iterator<Double> it = carrito.iterator();
		double numero;
		while(it.hasNext()) {
			numero = it.next();
			System.out.println(numero);
			numeroproductos++;
		}
		
		//CALCULO EL CAMBIO
		cambio = pagado - total;
		
		//MUESTRO POR PANTALLA LOS RESULTADOS
		System.out.println("Total IVA: "+totaliva);
		System.out.println("Precio bruto: "+total);
		System.out.println("Total mas IVA: "+totalmasiva);
		System.out.println("Numero total de productos: "+numeroproductos);
		System.out.println("Cantidad pagada: "+pagado);
		System.out.println("Cambio a devolver al cliente: "+cambio);
	}

}
