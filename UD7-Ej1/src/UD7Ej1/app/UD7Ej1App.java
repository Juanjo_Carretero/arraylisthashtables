package UD7Ej1.app;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class UD7Ej1App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DEFINICION DE VARIABLES
		int alumnos=3;
		double modul1=0;
		double modul2=0;
		double modul3=0;
		
		double medias[]=new double [alumnos];
		
		//LLAMADA A LA FUNCI�N DE RELLENAR ARRAY
		medias= rellenarArrayMedias(medias, modul1, modul2, modul3);
		
		//DEFINICION DE HASHTABLE
		Hashtable<String,Double> alumno=new Hashtable<String,Double>();
		
		alumno.put("39933333S", medias[0]);
		alumno.put("22222222F", medias[1]);
		alumno.put("71111111G", medias[2]);
		
		//MUESTRO EL CONTENIDO DE EL HASTABLE
		Enumeration<Double> enumeration = alumno.elements();
		Enumeration<String> llaves = alumno.keys();
		while(enumeration.hasMoreElements()&& llaves.hasMoreElements()) {
			System.out.println(""+"Valores: "+llaves.nextElement()+" " + enumeration.nextElement());
		}
		
	}
	
	public static double[] rellenarArrayMedias(double medias[], double modul1, double modul2, double modul3) {
		Scanner scan = new Scanner(System.in);
		for (int i = 0; i < medias.length; i++) {
			System.out.println("Nota del m�dulo 1 del alumno "+i);
			modul1 = scan.nextDouble();
			
			System.out.println("Nota del m�dulo 2 del alumno "+i);
			modul2 = scan.nextDouble();
			
			System.out.println("Nota del m�dulo 3 del alumno "+i);
			modul3 = scan.nextDouble();
			
			double media = calculoMedia(modul1, modul2, modul3);
			
			medias[i] = media;
			
		}
		return medias;
	}
	
	public static double calculoMedia(double modul1, double modul2, double modul3) {
		double media=0;
		
		media = (modul1 + modul2 + modul3)/3;
		
		return media;
	}
	
	//FUNCI�N DE MOSTRAR ARRAY
	public static void mostrarArray(double num[]) {
		for (int i = 0; i < num.length; i++) {
			System.out.println(num[i]);
		}
	}
}
