package UD7Ej4.app;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;
 
public class UD7Ej4App {
 
    public static void main(String[] args) {
    	
    	//DEFINO EL ARRAYLIST
    	ArrayList<Double> carrito = new ArrayList<>();
    	
    	//DEFINICION DE HASTABLE
		Hashtable<String, String> stock=new Hashtable<String, String>();
		
		stock.put("Port�til", "900�");
		stock.put("PC-Sobremesa", "2000�");
		stock.put("Tablet", "200�");
		stock.put("Smartphone", "550�");
		stock.put("Gr�fica NVIDIA", "700�");
		stock.put("Adaptador USB", "2�");
		stock.put("RAM", "50�");
		stock.put("Refrigeraci�n", "200�");
		stock.put("Cable ethernet", "10�");
		stock.put("Gr�fica AMD", "600�");
    	
        Scanner scan = new Scanner(System.in);
        boolean salir = false, salirventas = false;
        double pagado = 0, cambio=0, total=0, totaliva=0, totalmasiva=0;
        int opcion, iva = 0, numeroproductos = 0;
        
 
        while (!salir) {
 
            System.out.println("1. Gestionar Ventas");
            System.out.println("2. Consultar Ventas");
            System.out.println("3. A�adir un producto al stock");
            System.out.println("4. Mostrar la lista de stock");
            System.out.println("5. Busqueda de producto");
            System.out.println("6. Salir");
            try {
 
                System.out.println("Escribe una de las opciones");
                opcion = scan.nextInt();
 
                switch (opcion) {
                    case 1:
                        do {
                        	//MUESTRO EL STOCK PARA QUE EL CLIENTE COMPRE LO QUE DESEE
                        	mostrarStock(stock);
                        	System.out.println();
                        	
                			System.out.println("Precio de tu producto?");
                			double precio = scan.nextDouble();
                			
                			carrito.add(precio);
                			
                			System.out.println("Quiere comprar algun producto mas. (si o no)");
                			String decision = scan.next();
                			
                			if (decision.equals("si")) {
                				salirventas = true;
                			}else {
                				salirventas = false;
                				System.out.println("Cantidad pagada?");
                				pagado = scan.nextDouble();
                				
                				System.out.println("IVA (21 o 4 %)");
                				iva = scan.nextInt();
                			}
                			
                			total = total + precio;
                		} while (salirventas == true);
                        
                        //CALCULO EL IVA DEPENDIENDO DE SI ES AL 21% O AL 4%
                		if (iva==21) {
                			totaliva = (total * 21)/100;
                			totalmasiva = total + totaliva;
                		}else {
                			totaliva = (total * 4)/100;
                			totalmasiva = total + totaliva;
                		}
                		
                		System.out.println();
                        
                		//GENERO UNA ITERACI�N PARA PODER CONTAR EL NUMERO DE PRODUCTOS
                		Iterator<Double> it = carrito.iterator();
                		double numero;
                		while(it.hasNext()) {
                			numero = it.next();
                			System.out.println(numero);
                			numeroproductos++;
                		}
                		
                        //CALCULO EL CAMBIO
                		cambio = pagado - totalmasiva;
                		
                		System.out.println();
                		
                        break;
                        
                    case 2:
                    	//MUESTRO POR PANTALLA LOS RESULTADOS
                		System.out.println("Total IVA: "+totaliva);
                		System.out.println("Precio bruto: "+total);
                		System.out.println("Total mas IVA: "+totalmasiva);
                		System.out.println("Numero total de productos: "+numeroproductos);
                		System.out.println("Cantidad pagada: "+pagado);
                		System.out.println("Cambio a devolver al cliente: "+cambio);
                		System.out.println();
                		
                		System.out.println();
                        break;
                        
                    case 3:
                    	System.out.println("Nombre de producto.");
            			String nombreproducto = scan.next();
            			
            			System.out.println("Que precio tiene "+nombreproducto);
            			String precio = scan.next();
            			
            			stock.put(nombreproducto, precio);
            			
            			mostrarStock(stock);
            			
            			System.out.println();
                        break;
                        
                    case 4:
                        mostrarStock(stock);
                        
                        System.out.println();
                        break;
                        
                    case 5:
                    	System.out.println("Que producto quieres consultar");
            			String consulta = scan.next();
            			consultaProducto(consulta, stock);
            			
            			System.out.println();
                    	break;
                    	
                    case 6:
                    	salir = true;
                    	
                    	break;
                    default:
                        System.out.println("Solo n�meros entre 1 y 6");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un n�mero");
                scan.next();
            }
        }
 
    }
  //M�TODO PARA MOSTRAR EL STOCK
  	public static void mostrarStock(Hashtable<String, String> dicc) {
  		Enumeration<String> enumeration = dicc.elements();
  		Enumeration<String> llaves = dicc.keys();
  		while (enumeration.hasMoreElements()) {
  		  System.out.println(""+"Stock: " + llaves.nextElement() + " " + enumeration.nextElement());
  		}
  	}
  	
  //M�TODO PARA CONSULTAR UN PRODUCTO
  	public static void consultaProducto(String consulta, Hashtable<String, String> dicc) {
  		System.out.println("El producto consultado tiene el precio de: "+dicc.get(consulta));
  	}
 
}
